package com.stunningcoders.videochat.utils;

import javax.servlet.ServletOutputStream;
import javax.servlet.WriteListener;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

public class EncryptResponse extends HttpServletResponseWrapper {


    private HttpServletResponse httpServletResponse;
    private ByteArrayOutputStream byteArrayOutputStream;
    private ServletOutputStream servletOutputStream;
    private PrintWriter printWriter;

    /**
     * Constructs a response adaptor wrapping the given response.
     *
     * @param response The response to be wrapped
     * @throws IllegalArgumentException if the response is null
     */
    public EncryptResponse(HttpServletResponse response) {
        super(response);
        this.httpServletResponse = response;
        this.byteArrayOutputStream = new ByteArrayOutputStream();
        this.servletOutputStream = new SubServletOutputStream(byteArrayOutputStream);
        this.printWriter = new PrintWriter(new OutputStreamWriter(byteArrayOutputStream));

    }

    private class SubServletOutputStream extends ServletOutputStream {
        private ByteArrayOutputStream baos;

        SubServletOutputStream(ByteArrayOutputStream baos) {
            this.baos = baos;
        }

        @Override
        public void write(int b) {
            baos.write(b);
        }

        @Override
        public void write(byte[] b) {
            baos.write(b, 0, b.length);
        }

        @Override
        public boolean isReady() {
            return false;
        }

        @Override
        public void setWriteListener(WriteListener writeListener) {

        }
    }


    @Override
    public String getContentType() {
        return httpServletResponse.getContentType();
    }

    @Override
    public ServletOutputStream getOutputStream() {
        return servletOutputStream;
    }

    @Override
    public PrintWriter getWriter() {
        return printWriter;
    }


    public String getResponseBody() throws IOException {
        servletOutputStream.flush();
        printWriter.flush();
        return new String(byteArrayOutputStream.toByteArray(), this.getCharacterEncoding());
    }
}

package com.stunningcoders.videochat.utils;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

public class DecryptedRequest extends HttpServletRequestWrapper {

    private final HttpServletRequest encryptedRequest;
    private final String decryptedText;
    private final String contentType;

    public DecryptedRequest(HttpServletRequest encryptedRequest, String decryptedText) {
        this(encryptedRequest, decryptedText, encryptedRequest.getContentType());
    }


    private DecryptedRequest(HttpServletRequest encryptedRequest, String decryptedText, String contentType) {
        super(encryptedRequest);
        this.encryptedRequest = encryptedRequest;
        this.decryptedText = decryptedText;
        this.contentType = contentType;
    }


    @Override
    public ServletInputStream getInputStream() throws IOException {
        return new ServletInputStream() {

            private InputStream in = new ByteArrayInputStream(decryptedText.getBytes(encryptedRequest.getCharacterEncoding()));

            @Override
            public boolean isFinished() {
                return false;
            }

            @Override
            public boolean isReady() {
                return false;
            }

            @Override
            public void setReadListener(ReadListener readListener) {

            }

            @Override
            public int read() throws IOException {
                return in.read();
            }
        };
    }

    @Override
    public int getContentLength() {
        try {
            return decryptedText.getBytes(encryptedRequest.getCharacterEncoding()).length;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return super.getContentLength();
    }

    @Override
    public long getContentLengthLong() {
        try {
            return decryptedText.getBytes(encryptedRequest.getCharacterEncoding()).length;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return super.getContentLengthLong();
    }

    @Override
    public String getContentType() {
        return contentType;
    }
}

package com.stunningcoders.videochat.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stunningcoders.videochat.configuration.KeyStoreProperties;
import com.stunningcoders.videochat.data.CipherData;
import org.springframework.beans.factory.annotation.Autowired;
import sun.security.krb5.EncryptedData;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

public class EncryptionDecryptionUtils {


    private static PrivateKey privateKey; /*= "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAIWAWqXEx8HiuaN0LCxVs9BSzLqvEV/AU46Ts3vrP4CPAYtuXH9tFpiuoMSwdODqQr8lCtoYTAkY6F0YwKc+6PXfHhH8exKfTZOVE0Eyfu6PtiwuJexL3GhmTWS/60yis8At8jZO+IU50w7KjUewqW7dcUSA0k3L8DPa6E4YqZDbAgMBAAECgYAp0osTUXFO8Ss85KEOBo89XSMb/MpRl9vVV7orwQf0y1gtiRZbKR6iGFrvI2SKLXlZjk43AtOXKgFpYN7BxjiOkdYKETIUurgdOoyt4BxOL0Mm0kuX1bL+vLQzfi/B/UvMyJLnddfbgSld5a2FmOGJa+939yHsTTNGteelgu5ByQJBAMqJosfP3W0O9a5x1tX6DG/F789SaMyCiZXCl/GP4t4rHb0FLagtlV7/gfohX/4bJEZN8p6o2iQI8ndiXMkEp+8CQQCovaINGltg8VjtRxKYV8RXgiJK0iP07ENLDOYJ1LJJVKaGubxxyIN8pmY1sGfytPdd4cWWeg7hWmHhbiJbVJnVAkEAt/FIZS/jaaiseEHrW2o84UZjBWdSxmnHiruwQzLqV9vQplqNnK/JeP2UjhbBl4YO68rfDjlL5G47xzEOE4KnAQJAd4hF+FQ4cRbmsMcNFB/wNTE5S5lK4WBQVyTe/A3Q8gNmJ8ABDvVEt5NEijnZmOO666VYi70RN3AIMGINMndetQJBALkZC07pPE/X69iSEqLOQIiJX1S629p9Q/iJFpkX+LFWW/xseglN95HfHiC5c5Zy16zJPt4v+G42xCNzP8lW4p8=";*/

    private static PublicKey publicKey;/* = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCFgFqlxMfB4rmjdCwsVbPQUsy6rxFfwFOOk7N76z+AjwGLblx/bRaYrqDEsHTg6kK/JQraGEwJGOhdGMCnPuj13x4R/HsSn02TlRNBMn7uj7YsLiXsS9xoZk1kv+tMorPALfI2TviFOdMOyo1HsKlu3XFEgNJNy/Az2uhOGKmQ2wIDAQAB";*/

    public static SecretKey decryptSecretKey(String encryptedSecretKeyString) throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
       /* PKCS8EncodedKeySpec privateSpec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(privateKey));
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PrivateKey privateKey = keyFactory.generatePrivate(privateSpec);*/

        Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPWithSHA1AndMGF1Padding");
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        byte[] secretKeyBytes = cipher.doFinal(Base64.getDecoder().decode(encryptedSecretKeyString));
        return new SecretKeySpec(secretKeyBytes, 0, secretKeyBytes.length, "AES");
    }


    public static String decryptData(SecretKey secretKey, String encryptedData) throws BadPaddingException, IllegalBlockSizeException, NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException, JsonProcessingException {
        byte[] raw = secretKey.getEncoded();
        SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, skeySpec, new IvParameterSpec(new byte[16]));
        byte[] original = cipher.doFinal(Base64.getDecoder().decode(encryptedData));
        return new String(original, StandardCharsets.UTF_8);
    }

    public static <T> T getBody(HttpServletRequest request, Class<T> type) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(request.getReader(), type);
    }

    public static String encrypt(String responseBody) throws NoSuchAlgorithmException, IllegalBlockSizeException, InvalidKeyException, BadPaddingException, InvalidAlgorithmParameterException, NoSuchPaddingException, InvalidKeySpecException, JsonProcessingException {
        KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
        keyGenerator.init(128);
        SecretKey secretKey = keyGenerator.generateKey();
        String secretData = getEncryptedData(secretKey, responseBody);
        String encryptedSecretKey = getEncryptedKey(secretKey);
        CipherData cipherData = new CipherData();
        cipherData.setData(secretData);
        cipherData.setSecretKey(encryptedSecretKey);
        return new ObjectMapper().writeValueAsString(cipherData);
    }


    private static String getEncryptedData(SecretKey secretKey, String data) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        byte[] byteArray = new byte[16];
        cipher.init(
                Cipher.ENCRYPT_MODE,
                new SecretKeySpec(secretKey.getEncoded(), "AES"),
                new IvParameterSpec(byteArray)
        );
        return Base64.getEncoder()
                .encodeToString(cipher.doFinal(data.getBytes(StandardCharsets.UTF_8)));
    }

    private static String getEncryptedKey(SecretKey secretKey) throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        /*X509EncodedKeySpec publicSpec = new X509EncodedKeySpec(Base64.getDecoder().decode(publicKey));
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PublicKey publicKey = keyFactory.generatePublic(publicSpec);*/
        Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPWithSHA1AndMGF1Padding");
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
        return Base64.getEncoder().encodeToString(cipher.doFinal(secretKey.getEncoded()));
    }


    public static void loadKey(ClassLoader classLoader, KeyStoreProperties keyStoreProperties) throws KeyStoreException, UnrecoverableKeyException, NoSuchAlgorithmException, IOException, CertificateException {
        char[] password = keyStoreProperties.getPassword().toCharArray();
        KeyStore keystore = KeyStore.getInstance("PKCS12");
        InputStream keyFileStream = classLoader.getResourceAsStream(keyStoreProperties.getName());
        keystore.load(keyFileStream, password);
        privateKey = (PrivateKey) keystore.getKey(keyStoreProperties.getAlias(), password);
        Certificate cert = keystore.getCertificate(keyStoreProperties.getAlias());
        // Get public key
        publicKey = cert.getPublicKey();
    }
}

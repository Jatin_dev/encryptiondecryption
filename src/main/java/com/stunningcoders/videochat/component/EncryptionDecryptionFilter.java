package com.stunningcoders.videochat.component;

import com.stunningcoders.videochat.configuration.KeyStoreProperties;
import com.stunningcoders.videochat.data.CipherData;
import com.stunningcoders.videochat.utils.ClientChannel;
import com.stunningcoders.videochat.utils.DecryptedRequest;
import com.stunningcoders.videochat.utils.EncryptResponse;
import com.stunningcoders.videochat.utils.EncryptionDecryptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.crypto.SecretKey;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.GeneralSecurityException;

@Component
public class EncryptionDecryptionFilter implements Filter {

    @Autowired
    private KeyStoreProperties keyStoreProperties;

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        try {
            String channel = req.getHeader("channel");
            if (!ClientChannel.APP.toString().equals(channel)) {
                filterChain.doFilter(req, servletResponse);
                return;
            }
            EncryptionDecryptionUtils.loadKey(getClass().getClassLoader(), keyStoreProperties);
            CipherData cipherData = EncryptionDecryptionUtils.getBody(req, CipherData.class);
            SecretKey secretKey = EncryptionDecryptionUtils.decryptSecretKey(cipherData.getSecretKey());
            String modifiedRequest = EncryptionDecryptionUtils.decryptData(secretKey, cipherData.getData());
            System.out.println("Request filtered");
            DecryptedRequest decryptedRequest = new DecryptedRequest(req, modifiedRequest);
            HttpServletResponse res = (HttpServletResponse) servletResponse;
            EncryptResponse encryptResponse = new EncryptResponse(res);
            filterChain.doFilter(decryptedRequest, encryptResponse);
            System.out.println("Response filtered");
            String responseBod = encryptResponse.getResponseBody();
            System.out.println(responseBod);
            String responseBody = EncryptionDecryptionUtils.encrypt(responseBod);
            System.out.println(responseBody);
            res.setContentType(encryptResponse.getContentType()); // Be consistent with the request
            byte[] responseData = responseBody.getBytes(encryptResponse.getCharacterEncoding()); // The coding is consistent with the actual response
            res.setContentLength(responseData.length);
            ServletOutputStream out = res.getOutputStream();
            System.out.println(new String(responseData));
            out.write(responseData);
        } catch (GeneralSecurityException | IOException e) {
            e.printStackTrace();
        }
    }

}

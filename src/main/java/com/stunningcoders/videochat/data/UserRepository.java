package com.stunningcoders.videochat.data;

import com.stunningcoders.videochat.domain.User;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User, Long> {

    User findUserByEmail(String email);
}

package com.stunningcoders.videochat.configuration;

import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;

public class EncryptionDecryptionInterceptor implements ClientHttpRequestInterceptor {
    @Override
    public ClientHttpResponse intercept(HttpRequest httpRequest, byte[] bytes,
                                        ClientHttpRequestExecution clientHttpRequestExecution) throws IOException {

        ClientHttpResponse response = clientHttpRequestExecution.execute(httpRequest, bytes);
        response.getBody();

        return clientHttpRequestExecution.execute(httpRequest, bytes);
    }
}


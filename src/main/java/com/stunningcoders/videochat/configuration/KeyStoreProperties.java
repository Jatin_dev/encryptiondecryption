package com.stunningcoders.videochat.configuration;

import com.stunningcoders.videochat.factory.YAMLPropertySourceFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ConfigurationProperties(prefix = "keystore")
@PropertySource(value = "classpath:application.yaml", factory = YAMLPropertySourceFactory.class)
public class KeyStoreProperties {

    private String name;

    private String alias;

    private String password;

    public String getName() {
        return name;
    }

    public String getAlias() {
        return alias;
    }

    public String getPassword() {
        return password;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

package com.stunningcoders.videochat;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.stunningcoders.videochat.data.CipherData;
import com.stunningcoders.videochat.utils.EncryptionDecryptionUtils;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import sun.rmi.runtime.Log;

import javax.crypto.SecretKey;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.logging.Logger;

@SpringBootApplication
public class DemoApplication {


    @Value("${amqp.queue.name}")
    private String queueName;

    @Value("${amqp.exchange.name}")
    private String exchangeName;

    @Bean
    public Queue queue() {
        return new Queue(queueName, false);
    }

    @Bean
    public TopicExchange topicExchange() {
        return new TopicExchange(exchangeName);
    }

    @Bean
    public Binding binding(Queue queue, TopicExchange topicExchange) {
        return BindingBuilder.bind(queue).to(topicExchange).with(queueName);
    }

    public static void main(String[] args) {
        try {
            System.out.println("Before run");
            SpringApplication.run(DemoApplication.class, args);
            System.out.println("After run");
            if (false) {
                createRsaKeys();
            }
//            test();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void test() {
        String text = "qwertyuiopasdfghjkl";
        try {
            String encrypt = EncryptionDecryptionUtils.encrypt(text);
            ObjectMapper objectMapper = new ObjectMapper();
            CipherData d = objectMapper.readValue(encrypt, CipherData.class);
            SecretKey secretKey1 = EncryptionDecryptionUtils.decryptSecretKey(d.getSecretKey());
            String modifiedRequest1 = EncryptionDecryptionUtils.decryptData(secretKey1, d.getData());
            System.out.println(modifiedRequest1);

        } catch (GeneralSecurityException | IOException e) {
            e.printStackTrace();
        }

    }

    private static void createRsaKeys() throws NoSuchAlgorithmException {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(1024);
        KeyPair keyPair = keyPairGenerator.genKeyPair();
        String privateKeyString = Base64.getEncoder().encodeToString(keyPair.getPrivate().getEncoded());
        String publicKeyString = Base64.getEncoder().encodeToString(keyPair.getPublic().getEncoded());

        System.out.println("rsa key pair generated");
        System.out.println("private key : " + privateKeyString);
        System.out.println("public key : " + publicKeyString);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}

package com.stunningcoders.videochat.controller;


import com.stunningcoders.videochat.configuration.ResponseWrapper;
import com.stunningcoders.videochat.domain.User;
import com.stunningcoders.videochat.service.UserDetailsServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class AuthController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthController.class);

    @Value("${amqp.queue.name}")
    private String queueName;


    private final UserDetailsServiceImpl userDetailsService;

  /*  private final RestTemplate restTemplate;

    private final ConfigurableApplicationContext configurableApplicationContext;

    private final ObjectMapper objectMapper;

    private final RabbitTemplate rabbitTemplate;*/


    @Autowired
    public AuthController(UserDetailsServiceImpl userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @PostMapping("/register")
    public @ResponseBody
    ResponseWrapper<String> createUser(@RequestBody User user) {
        System.out.println(user.toString());
        User existingUser = userDetailsService.findUserByEmail(user.getEmail());
        if (existingUser != null) {
            return ResponseWrapper.getSuccessResponse("User already exist");
        } else {
            userDetailsService.saveUser(user);
            return ResponseWrapper.getSuccessResponse("success");
        }
    }


    @PostMapping("/invade")
    public @ResponseBody
    ResponseWrapper<String> invade(@RequestBody User user) {
        UserDetails existingUser = userDetailsService.loadUserByUsername(user.getEmail());
        LOGGER.info("Sending message " + existingUser.toString());

        String status = "";

        if (userDetailsService.isPasswordValid(user.getPassword(), existingUser.getPassword())) {
            status = "Success";
        } else {
            status = "Invalid Password";
        }
        return ResponseWrapper.getSuccessResponse(status);
    }
}

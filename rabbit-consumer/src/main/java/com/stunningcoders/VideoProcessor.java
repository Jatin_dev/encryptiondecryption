package com.stunningcoders;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class VideoProcessor {

    private final ObjectMapper objectMapper;
    private static final Logger LOGGER = LoggerFactory.getLogger(VideoProcessor.class);

    @Autowired
    public VideoProcessor(ObjectMapper objectMapper) {
        super();
        this.objectMapper = objectMapper;
    }

    public void receiveMessage(String videoChatJson) {
        LOGGER.info("Message received");
        try {
            VideoChat videoChat = objectMapper.readValue(videoChatJson, VideoChat.class);
            LOGGER.info("Video chat message received" + videoChat.getChatId());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
